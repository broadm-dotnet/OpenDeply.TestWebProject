﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OpenDeploy.TestWebProject.WebApi
{
    public class UserController : ApiController
    {
        // GET api/user/5
        public string Get(int id)
        {
            return $"get user by Id: {id}";
        }
    }
}